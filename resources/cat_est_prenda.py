# Catalogo del los estados de una prenda(Excelente, mal estado...)

from flask_restful import Resource
from db import DatabaseConnection

class Cat_est_prenda(Resource):
    def get(self):
        df =  DatabaseConnection().query_df("""
        SELECT id_cat_est_prenda, estado_prenda, porc_aforo
        FROM sc_alab.cat_est_prenda;
        """)
        
        return df.to_dict(orient='records')