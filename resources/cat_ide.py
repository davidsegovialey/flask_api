# catalago de indentificaciones

from flask import json, request
from flask_restful import Resource
from marshmallow import fields
from marshmallow import Schema
from marshmallow.exceptions import ValidationError

from db import DatabaseConnection

class IdeSchema(Schema):
    name = fields.Str(required=True)


class Cat_identificaciones(Resource):
    def get(self):
        df = DatabaseConnection().query_df("SELECT * FROM sc_alab.cat_identificaciones;")
        result_code = 200
        return json.loads(df.to_json(orient='records')), result_code


    def post(self):
        data = request.get_json()

        try:
            is_valid = IdeSchema().load(data)
            if is_valid:
                # do something more here...
                pass
        except ValidationError as err:
            error_messages = json.dumps(err.messages)
            return {"success": False, "status_message":json.loads(error_messages)}, 400

        conn = DatabaseConnection()
        name = data['name']
        df = conn.query_df("""SELECT nom_ide
        FROM sc_alab.cat_identificaciones WHERE nom_ide = %s""", (name,))

        if (not df.empty):
            return {"success": False, "status_message": "Registro ya existente en la bd."}, 409

        DatabaseConnection().query("""INSERT INTO sc_alab.cat_identificaciones
        (nom_ide) VALUES(%s);""", (data['name'],))

        return {"success": True}, 201