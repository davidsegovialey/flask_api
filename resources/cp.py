from flask import request
from flask_restful import Resource
from db import DatabaseConnection

class Cp(Resource):
  def get(self):
    query = request.args.get("query")

    if query == None:
      return{}

    if len(query) == 0:
      return{}

    df = DatabaseConnection().query_df("""
    select c.id_ref_cp, c.id_cat_asentamientos, c.id_municipio, c.cp, c.nom_entidad,
    m.id_municipio, e.id_estado, ca.nom_asentamientos, e.nom_estado
    from alab_emp.sc_alab.codigopostal c
    inner join alab_emp.sc_alab.cat_asentamientos ca
    on ca.id_cat_asentamientos = c.id_cat_asentamientos
    inner join sc_alab.municipios m on m.id_municipio = c.id_municipio
    inner join sc_alab.estados e on e.id_estado = m.id_estado
    where c.cp = %s; """, (query,))

    return df.to_dict(orient="records"), 200

