# Catalogo de articulos

from flask import request
from flask_restful import Resource
from db import DatabaseConnection


class ItemSearch(Resource):
    def get(self):
        query = request.args.get("query")
        if query == None:
            return {}

        if len(query) == 0:
            return {}

        df = DatabaseConnection().query_df(f"""
        SELECT cc.nom_categoria, dp.id_detalle_prenda, dp.marca, dp.modelo, 
        dp.ram, dp.almacenamiento, dp.monto_aforo 
        FROM sc_alab.detalle_prenda dp 
        INNER JOIN sc_alab.cat_categorias cc 
        on dp.id_cat_categoria = cc.id_cat_categoria 
        WHERE concat_ws
        (' ',dp.marca, dp.modelo, dp.ram, dp.almacenamiento, cc.nom_categoria) 
        ilike '%%{query}%%'""")  # -__-

        return df.to_dict(orient='records')
