# batch, pero sin usar una tabla para la fecha

import json
import pandas as pd
from flask_restful import request, Resource
from marshmallow import Schema, ValidationError, fields
from db import DatabaseConnection
from dateutil import rrule
from datetime import datetime, timezone


class Batch2Schema(Schema):
    fecha_corte = fields.DateTime(required=True)


class Batch2(Resource):
    def post(self):
        json_data = request.get_json()

        try:
            data = Batch2Schema().load(json_data)
        except ValidationError as err:
            error_messages = json.dumps(err.messages)
            return {"success": False, "status_message": json.loads(error_messages)}, 400

        conn = DatabaseConnection()
        conn.open_connection()
        # Retorna:
        # id_boleta, mont_prest_total, mont_prest_total + refrendo, pagos, mont_prest_total - pagos
        boletas_activas_df = pd.read_sql(
            """
           SELECT
                b.id_boleta, 
                b.fecha_alta,
                periodo, 
                mont_prest_total, 
                mont_prest_total + mont_prest_total * interes AS deuda, 
                coalesce(pago_total, 0.0) AS pago_total, 
                (mont_prest_total + mont_prest_total * interes) - pago_total AS adeudo
            FROM sc_alab.boletas b
            LEFT JOIN (
                select 
                pe.id_producto_emp, 
                pe.tasa_interes as interes,
                pe.periodo as periodo
                from sc_alab.producto_emp pe
            ) tasa
            on tasa.id_producto_emp = b.id_producto_emp 
            LEFT JOIN (
                SELECT rp.id_boleta , SUM(rp.monto) AS pago_total
                FROM sc_alab.registro_pago rp 
                GROUP BY id_boleta 
            ) pagos
            ON b.id_boleta = pagos.id_boleta
            WHERE b.id_cat_stats_bol = 1
            """,
            conn.conn
        )

        boletas_vencidas = []
        boletas_desempeñadas = []

        for _, row in boletas_activas_df.iterrows():
            row['deuda'] = round(row['deuda'], 2)
            starter_date = row['fecha_alta'].date()
            end_date = data['fecha_corte']
            timestamp = end_date.replace(tzinfo=timezone.utc).timestamp()
            end_date = datetime.fromtimestamp(timestamp)

            month_diff = len(list(rrule.rrule(rrule.MONTHLY,
                  dtstart=starter_date, until=end_date))) - 1
            

            if month_diff > 0:
                monthly_payment = round(row['deuda'] / row['periodo'], 2)
                must_amount = monthly_payment * month_diff

                if row['pago_total'] < must_amount:
                    boletas_vencidas.append(int(row['id_boleta']))
                elif row['pago_total'] >= row['deuda']:
                    boletas_desempeñadas.append(int(row['id_boleta']))


        conn.cursor.execute(
            """
            UPDATE sc_alab.boletas
            SET id_cat_stats_bol = 3
            WHERE id_boleta = ANY(%s);
            """,
            (boletas_vencidas,)
        )

        conn.cursor.execute(
            """
            UPDATE sc_alab.boletas
            SET id_cat_stats_bol = 2
            WHERE id_boleta = ANY(%s);
            """,
            (boletas_desempeñadas,)
        )

        conn.close_connection()

        return {}, 204
