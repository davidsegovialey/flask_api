# Retorna datos de la boleta

import json
from flask_restful import Resource, request
from db import DatabaseConnection
from marshmallow import Schema, fields, ValidationError


class TicketPutSchema(Schema):
    id_cat_stats_bol = fields.Int(required=True)


class Ticket(Resource):
    def get(self, id):
        conn = DatabaseConnection()
        df_boleta = conn.query_df(
            """
            SELECT b.id_boleta, b.id_producto_emp, b.id_persona, b.id_cat_stats_bol,
            b.fecha_alta, b.fecha_fin, b.mont_prest_total, pe.tasa_interes, pe.periodo,
            csb.nom_stats_bol 
            FROM sc_alab.boletas b 
            INNER JOIN sc_alab.producto_emp pe ON pe.id_producto_emp = b.id_producto_emp
            INNER JOIN sc_alab.cat_status_boleta csb ON csb.id_cat_stats_bol = b.id_cat_stats_bol 
            WHERE b.id_boleta = %s
            """, (id,)
        )

        if df_boleta.empty:
            return {}, 404

        dict_detalle_prendas = conn.query_df(
            """
            SELECT db.id_prenda, db.monto_prestamo, dp.monto_aforo, dp.marca,
            dp.modelo, dp.ram, dp.almacenamiento,
            p.id_cat_est_prenda, p.id_detalle_prenda, cc.nom_categoria,
            cep.estado_prenda, cep.porc_aforo 
            FROM sc_alab.detalle_boleta db
            INNER JOIN sc_alab.prendas p on p.id_prenda = db.id_prenda 
            INNER JOIN sc_alab.detalle_prenda dp on dp.id_detalle_prenda = p.id_detalle_prenda
            INNER JOIN sc_alab.cat_categorias cc on cc.id_cat_categoria = dp.id_cat_categoria
            INNER JOIN sc_alab.cat_est_prenda cep on cep.id_cat_est_prenda = p.id_cat_est_prenda 
            WHERE db.id_boleta = %s
            """, (id,)
        ).to_dict(orient='records')

        df_boleta['fecha_alta'] = df_boleta['fecha_alta'].astype(str)
        df_boleta['fecha_fin'] = df_boleta['fecha_fin'].astype(str)

        result = df_boleta.to_dict(orient='records')[0]
        result.update({'prendas': dict_detalle_prendas })

        return result, 200


    def put(self, id):
        json_data = request.get_json()
        try:
            data = TicketPutSchema().load(json_data)
        except ValidationError as err:
            error_messages = json.dumps(err.messages)
            return {"success": False, "status_message":json.loads(error_messages)}, 400

        DatabaseConnection().query(
            """
            UPDATE sc_alab.boletas
            SET id_cat_stats_bol=%s
            WHERE id_boleta=%s
            """, (
                data['id_cat_stats_bol'],
                id,
            )
        )

        return {}, 204