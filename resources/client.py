# Clientes (persona)

from flask import request, json
from flask_restful import Resource
from marshmallow import fields
from marshmallow.decorators import validates_schema
from marshmallow.schema import Schema
from marshmallow.exceptions import ValidationError
from db import DatabaseConnection
from common.utils import build_cors_preflight_response
import pandas


class EmailSchema(Schema):
    dir_email = fields.Email(required=True)
    ver_email = fields.Bool(required=False)


class DomSchema(Schema):
    id_ref_cp = fields.Int(required=True)
    tipo_domicilio = fields.Str(required=True)
    nom_direccion = fields.Str(required=True)
    no_ext = fields.Str(required=True)
    no_int = fields.Str(required=False, allow_none=True, load_default=None, dump_default=None)
    vigencia = fields.Bool(required=True)


class PhoneSchema(Schema):
    tipo_tel = fields.Str(required=True)
    no_tel = fields.Int(required=True)


class ClientSchema(Schema):
    nom_1 = fields.Str(required=True)
    nom_2 = fields.Str(required=False, allow_none=True, load_default=None, dump_default=None)
    apellido_pat = fields.Str(required=True)
    apellido_mat = fields.Str(required=True)
    edad = fields.Int(required=True)
    numero_ide = fields.Str(required=True)
    id_cat_ide = fields.Int(required=True)

    correos = fields.List(fields.Nested(EmailSchema))
    domicilios = fields.List(fields.Nested(DomSchema))
    telefonos = fields.List(fields.Nested(PhoneSchema))

    @validates_schema
    def validate_schema(self, data, **kwargs):
        if len(data['numero_ide']) == 0:
            raise ValidationError("numero_ide no puede ser vacio.", "numero_ide")

        if len(data['nom_1']) == 0:
            raise ValidationError("nom1 no puede ser vacio.", "nom1")

        if len(data['apellido_pat']) == 0:
            raise ValidationError("nom1 no puede ser vacio.", "apellido_pat")

        if len(data['apellido_mat']) == 0:
            raise ValidationError("nom1 no puede ser vacio.", "apellido_mat")




class Client(Resource):
    def options(self):
        return build_cors_preflight_response()


    def get(self, id):
        conn = DatabaseConnection()
        conn.open_connection()

        df_persona = pandas.read_sql("""
        SELECT p.id_persona, p.id_cat_ide, p.nom_1, p.nom_2, p.apellido_pat,
        p.apellido_mat, p.edad, p.numero_ide, ci.nom_ide 
        FROM sc_alab.personas p 
        INNER JOIN sc_alab.cat_identificaciones ci on ci.id_cat_ide = p.id_cat_ide
        WHERE p.numero_ide = %s
        """, conn.conn, params=(id,))

        if df_persona.empty:
            return {"status_message": "Cliente no existente."}, 404

        id_persona = int(df_persona['id_persona'][0])

        dict_domicilios = pandas.read_sql("""
        SELECT pd.id_persona_dom, pd.id_ref_cp, pd.tipo_domicilio, pd.nom_direccion,
        pd.no_ext, pd.no_int, pd.vigencia, c.id_cat_asentamientos, c.id_municipio,
        c.cp, c.nom_entidad, m.id_estado, m.nom_municipio, e.nom_estado,
        ca.nom_asentamientos 
        FROM sc_alab.persona_domicilio pd
        INNER JOIN sc_alab.codigopostal c on c.id_ref_cp = pd.id_ref_cp
        INNER JOIN sc_alab.municipios m on m.id_municipio = c.id_municipio
        INNER JOIN sc_alab.estados e on e.id_estado = m.id_estado
        INNER JOIN sc_alab.cat_asentamientos ca on ca.id_cat_asentamientos = c.id_cat_asentamientos 
        WHERE pd.id_persona = %s
        """, conn.conn, params=(id_persona,)).to_dict(orient='records')

        dict_telefonos = pandas.read_sql("""
        SELECT t.id_telefono, t.tipo_tel, t.no_tel from sc_alab.telefonos t 
        INNER JOIN sc_alab.personas p on t.id_persona = p.id_persona 
        WHERE t.id_persona = %s
        """, conn.conn, params=(id_persona,)).to_dict(orient='records')

        dict_emails = pandas.read_sql("""
        SELECT id_email, ver_email, dir_email from sc_alab.email e
        INNER JOIN sc_alab.personas p on p.id_persona = e.id_email 
        WHERE e.id_persona = %s
        """, conn.conn, params=(id_persona,)).to_dict(orient='records')

        # append...
        result = df_persona.to_dict(orient='records')[0]
        result.update({"correos": dict_emails})
        result.update({"telefonos": dict_telefonos})
        result.update({"domicilios": dict_domicilios})

        conn.close_connection()

        return result


    def post(self):
        request_data = request.get_json()
        
        try:
            data = ClientSchema().load(request_data)
        except ValidationError as err:
            error_messages = json.dumps(err.messages)
            return {"success": False, "status_message":json.loads(error_messages)}, 400

        conn = DatabaseConnection()
        conn.open_connection()

        # validar si el cliente ya existe en la bd
        df = pandas.read_sql_query("""SELECT numero_ide 
        FROM sc_alab.personas WHERE numero_ide = %s""", conn.conn, params=(data['numero_ide'],))

        if not df.empty:
            return {"success": False, "status_message": "Registro ya existente en la bd."}, 409
 
        id_persona = conn.cursor.execute("""
            INSERT INTO sc_alab.personas
            (id_cat_ide, nom_1, nom_2, apellido_pat, apellido_mat, edad, numero_ide)
            VALUES(%s, %s, %s, %s, %s, %s, %s) RETURNING id_persona;""", 
        (
            data['id_cat_ide'],
            data['nom_1'],
            data['nom_2'],
            data['apellido_pat'],
            data['apellido_mat'],
            data['edad'],
            data['numero_ide']
        ))

        # id de la persona que recien insertamos
        id_persona = conn.cursor.fetchone()[0]

        for email in data['correos']:
            conn.cursor.execute("""
            INSERT INTO sc_alab.email
            (id_persona, ver_email, dir_email)
            VALUES(%s, false, %s);
            """, 
            (
                id_persona,
                email['dir_email']
            ))

        for tel in data['telefonos']:
            conn.cursor.execute("""
            INSERT INTO sc_alab.telefonos
            (id_persona, tipo_tel, no_tel)
            VALUES(%s, %s, %s);
            """,
            (
                id_persona,
                tel['tipo_tel'],
                tel['no_tel']
            ))

        # insertar los domicilios...
        for dom in data['domicilios']:
            conn.cursor.execute("""
            INSERT INTO sc_alab.persona_domicilio
            (id_persona, id_ref_cp, tipo_domicilio, nom_direccion,
             no_ext, no_int, vigencia)
            VALUES(%s, %s, %s, %s, %s, %s, %s);
            """,
            (
                id_persona,
                dom['id_ref_cp'],
                dom['tipo_domicilio'],
                dom['nom_direccion'],
                dom['no_ext'],
                dom['no_int'],
                dom['vigencia']
            ))

        conn.close_connection()
        result = {"success": True, "cliente_id": id_persona}, 201

        return result
