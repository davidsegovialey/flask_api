# Busqueda de clientes dado un termino de busqueda

from flask import request
from flask_restful import Resource
from db import DatabaseConnection

class ClientSearch(Resource):
    def get(self):
        query = request.args.get("query")

        if query == None:
            return {}

        if len(query) == 0:
            return {}

        df = DatabaseConnection().query_df(f"""
        SELECT p.id_persona, numero_ide, concat_ws(' ', p.nom_1, p.nom_2, p.apellido_pat, p.apellido_mat) as nombre_completo
        FROM sc_alab.personas p
        WHERE concat_ws(' ', p.nom_1, p.nom_2, p.apellido_pat, p.apellido_mat, p.numero_ide) 
        ilike '%{query}%';""")

        return df.to_dict(orient="records"), 200