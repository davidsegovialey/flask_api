from datetime import datetime
from typing_extensions import TypeVarTuple
from flask import request, json
from flask_restful import Resource
from marshmallow import fields
from marshmallow.schema import Schema
from marshmallow.exceptions import ValidationError
from db import DatabaseConnection


class PaymentSchema (Schema):
    fecha_inicial = fields.DateTime(required=True)
    fecha_limite = fields.DateTime(requiered=True)
    monto_prestamo = fields.float(required=True)
    tasa_interes = 2.5
    plazos = fields.Int(required=True)

class Detalle_prenda(Resource):
    data = request.get_json()

    def get(self):
        df =  DatabaseConnection().query_df("""
        SELECT fecha_inicial, fecha limite, monto_prestamo_total, tasa_interes, plazos
        FROM sc_alab.id_producto_emp;
        """)

        result_code = 200
        return json.loads(df.to_json(orient='records')), result_code



