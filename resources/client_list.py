# Lista para la tabla de clientes
from flask_restful import Resource
from db import DatabaseConnection


class ClientList(Resource):
    def get(self):
        df_personas = DatabaseConnection().query_df(
            """
            SELECT p.id_persona, p.id_cat_ide, p.nom_1, p.nom_2, p.apellido_pat,
            p.apellido_mat, p.edad, p.numero_ide
            FROM sc_alab.personas p
            """
        )

        return df_personas.to_dict(orient="records"), 200
