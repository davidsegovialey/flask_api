# Auntenticacion 

from flask import json, request
from flask_restful import Resource
from marshmallow.exceptions import ValidationError

from db import DatabaseConnection
from marshmallow import Schema, fields

class AuthSchema(Schema):
    username = fields.Str(required=True)
    password = fields.Str(required=True)


class Auth(Resource):
    def post(self):
        request_data = request.get_json()
        
        try:
            is_valid = AuthSchema().load(request_data)
            if is_valid:
                # do something more here...
                pass
        except ValidationError as err:
            error_messages = json.dumps(err.messages)
            return {"success": False, "status_message":json.loads(error_messages)}, 400
            
        df = DatabaseConnection().query_df(
            """SELECT
            id_personal,
            p.id_cargo,
            nombre,
            c.nom_cargo
            FROM sc_personal.personal as p
            INNER JOIN sc_personal.cargos c
            on p.id_cargo = c.id_cargo
            WHERE usuario = %s
            AND contraseña  = %s;""", (request_data['username'], request_data['password'])) 
        
        if (df.empty):
            return {}, 401

        user_data = json.loads(df.to_json(orient='records'))
        
        return user_data[0]
