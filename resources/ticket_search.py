# Busqueda de boletas

from flask import request
from flask_restful import Resource
from db import DatabaseConnection


class TicketSearch(Resource):
    def get(self):
        query = request.args.get("query")

        if query == None:
            return {}

        if len(query) == 0:
            return {}

        df = DatabaseConnection().query_df(f"""
        SELECT p.id_persona, 
        concat_ws(' ',p.nom_1, p.nom_2, p.apellido_pat, p.apellido_mat) 
        as nombre_completo, b.id_boleta, b.mont_prest_total, b.fecha_alta, b.fecha_fin 
        FROM sc_alab.personas p inner join sc_alab.boletas b 
        ON p.id_persona = b.id_persona 
        WHERE concat_ws(' ',b.id_boleta, p.nom_1, p.nom_2, p.apellido_pat, p.apellido_mat) 
        ilike '%{query}%' and b.id_cat_stats_bol = 1""")

        df['fecha_alta'] = df['fecha_alta'].astype(str)
        df['fecha_fin'] = df['fecha_fin'].astype(str)

        return df.to_dict(orient="records")