
# item...

from flask import request
from flask_restful import Resource
from db import DatabaseConnection

class ItemDetail(Resource):
    def get(self, id):
        
        df = DatabaseConnection().query_df("""
        SELECT cc.nom_categoria, dp.id_detalle_prenda, dp.marca, dp.modelo, 
        dp.ram, dp.almacenamiento, dp.monto_aforo 
        FROM sc_alab.detalle_prenda dp 
        INNER JOIN sc_alab.cat_categorias cc 
        on dp.id_cat_categoria = cc.id_cat_categoria 
        WHERE dp.id_detalle_prenda = %s
        """, params=(id,) )

        return df.to_dict(orient='records')
