# Clase que se encarga de registrar y devolver datos de empeño

from flask import json
from flask_restful import Resource, request
from marshmallow import Schema, fields
from marshmallow.exceptions import ValidationError
from marshmallow.utils import EXCLUDE
from numpy import int64
from db import DatabaseConnection
from datetime import date
from dateutil.relativedelta import relativedelta
from urllib.parse import unquote_plus

class PledgeSchema(Schema):
    montos_aprobados = fields.List(fields.Float(), required=True)


class PostPledgePrendasSchema(Schema):
    class Meta:
        unknown  = EXCLUDE
    id_detalle_prenda = fields.Int(required=True)
    id_cat_est_prenda = fields.Int(required=True)
    monto_prestamo = fields.Float(required=True)


class PostPledgeSchema(Schema):
    class Meta:
        unknown  = EXCLUDE
    numero_ide = fields.Str(required=True)
    id_producto_emp = fields.Int(required=True)
    mont_prest_total = fields.Float(required=True)
    fecha_alta = fields.DateTime(required=True, format="iso")
    fecha_fin = fields.DateTime(required=True)
    prendas = fields.List(fields.Nested(PostPledgePrendasSchema), required=True)


class Pledge(Resource):
    def get(self):

        id_producto_emp = request.args.get("prod")
        montos_json = unquote_plus(request.args.get("montos"))
        montos_json = json.loads(montos_json)

        try:
            PledgeSchema().load(montos_json)
        except ValidationError as err:
            error_messages = json.dumps(err.messages)
            return {"success": False, "status_message":json.loads(error_messages)}, 400


        df = DatabaseConnection().query_df("""
        SELECT * FROM sc_alab.producto_emp pe
        WHERE pe.id_producto_emp = %s
        """, (id_producto_emp,))

        if df.empty:
            return {}, 400

        monto_prestamo_total = 0
        for i in montos_json['montos_aprobados']:
            monto_prestamo_total = i + monto_prestamo_total

        now = date.today()
        future_date = now + relativedelta(months=df['periodo'][0])
        fecha_fin = future_date.strftime('%Y-%m-%d')
        fecha_alta = now.strftime('%Y-%m-%d')

        data = {
            "fecha_alta": fecha_alta,
            "fecha_fin": fecha_fin,
            "monto_prestamo_total": monto_prestamo_total,
        }

        for i in df.keys():
            data[i] = df[i][0]
            if type(data[i]) == int64:
                data[i] = int(data[i])

        return data, 200


    def post(self):
        json_data = request.get_json()

        try:
            data = PostPledgeSchema().load(json_data, unknown=EXCLUDE)
        except ValidationError as err:
            error_messages = json.dumps(err.messages)
            return {"success": False, "status_message":json.loads(error_messages)}, 400

        conn = DatabaseConnection()
        df_persona = conn.query_df(
            """
            SELECT id_persona
            FROM sc_alab.personas p where p.numero_ide = %s;
            """, (data["numero_ide"],)
        )

        if df_persona.empty:
            return {}, 400

        id_persona = int(df_persona['id_persona'][0])

        conn.open_connection()
        conn.cursor.execute(
            """
            INSERT INTO sc_alab.boletas
            (id_producto_emp, id_persona, id_cat_stats_bol, fecha_alta, 
            fecha_fin, mont_prest_total)
            VALUES(%s, %s, %s, %s, %s, %s) RETURNING id_boleta;
            """, 
            (
                data["id_producto_emp"],
                id_persona,
                1,  # hardcode - Activa
                data["fecha_alta"],
                data["fecha_fin"],
                data["mont_prest_total"]
            )
        )

        id_boleta = conn.cursor.fetchone()[0]

        for prenda in data["prendas"]:
            conn.cursor.execute(
                """
                INSERT INTO sc_alab.prendas
                (id_cat_est_prenda, id_detalle_prenda)
                VALUES(%s, %s) RETURNING id_prenda;
                """,
                (
                    prenda["id_cat_est_prenda"],
                    prenda["id_detalle_prenda"],
                )
            )

            id_prenda = conn.cursor.fetchone()[0]

            conn.cursor.execute(
                """
                INSERT INTO sc_alab.detalle_boleta
                (id_boleta, id_prenda, monto_prestamo)
                VALUES(%s, %s, %s);
                """, 
                (
                    id_boleta,
                    id_prenda,
                    prenda["monto_prestamo"],
                )
            )

        conn.close_connection()
        return {
            "success": True,
            "id_boleta": id_boleta,
        }, 200