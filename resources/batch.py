#{"fecha_corte":"2021-00-00 00:00:00.000"}
# proceso batch
from marshmallow.exceptions import ValidationError
from flask import request, json
from flask_restful import Resource
from marshmallow import fields
from marshmallow.schema import Schema
from db import DatabaseConnection
from marshmallow.exceptions import ValidationError
from marshmallow import fields
from marshmallow.schema import Schema
from flask import request, json

#esquema para validar si llego una fecha
class avanzar_fecha (Schema):
    fecha_corte = fields.DateTime(required=False)

#Funcion para obtener la fecha de caducidad de boletas mayores a la fecha actual
def fechaCadBol(conex):
    fechasFin=conex.query_df("""
        select b.id_boleta, b.fecha_fin from sc_alab.boletas b
	    where b.fecha_fin > (
        select fa.fecha_actual from sc_alab.fecha_actual fa)                  
        """)
    
    return fechasFin

#Funcion para comparar la fecha actual de la DB y obtener la fecha fin mas cercana de las boletas
def compararFecha(conex, fechasFin):
        fechaAct=fechasFin.values[0][1]
        for x in fechasFin.values:        
            nuevaFecha=conex.query_df("""
            select b.id_boleta, b.fecha_fin from sc_alab.boletas b
	        where b.fecha_fin < '%s' 
            and b.id_boleta = %s ;              
            """,({x[0]},fechaAct))
            if not nuevaFecha.empty:
                fechaAct=nuevaFecha.values[0][1] 
        return fechaAct
 
#Funcion para actualizar la fecha actual de la bd            
def actFecha(conex,fechaAct,bandera):
    if bandera==1:
        conex.query("""update sc_alab.fecha_actual
            set fecha_actual='%s';""",(fechaAct))
    else:
        conex.query("""update sc_alab.fecha_actual
            set fecha_actual='%s'::TIMESTAMP + '1 S'::interval;""",(fechaAct))
        
#Funcion para obtener las boletas caducas aun activas
def bolCaducas(conex):
    bolVen = conex.query_df("""
        select b.id_boleta, b.fecha_fin, b.id_cat_stats_bol, mont_prest_total
        from sc_alab.boletas b
        where  b.fecha_fin < (select fa.fecha_actual from sc_alab.fecha_actual fa) 
        and b.id_cat_stats_bol =1;""")
    
    return bolVen

#Funcion para cambiar el status de la boleta
def actStatBol(conex,bolVen):
    for i in bolVen.values:
            conex.query("""
            update sc_alab.boletas set id_cat_stats_bol = case 
            when (id_boleta = %s) 
            and (mont_prest_total > (
	        select sum(rp.monto)
	        from sc_alab.registro_pago rp
	        inner join sc_alab.boletas b on b.id_boleta = rp.id_boleta
	        where b.id_boleta = %s
            )) then 3
            else 2
            end
            where id_boleta = %s        
            """, (i[0], i[0], i[0]))

#clase principal
class batch(Resource):
    def post(self):
        #obtiene un json que se manda desde el front
        request_data = request.get_json()
        
        #valida si el json cumple con los campos requeridos
        try:
            data = avanzar_fecha().load(request_data)
        
        #Menejo de excepciones
        except ValidationError as err:
            error_messages = json.dumps(err.messages)
            return {"success": False, "status_message":json.loads(error_messages)}, 400
        
        #Conexion a la DB y variables
        conex = DatabaseConnection()
        fechasFin=fechaAct=bolVen=bandera=''  
        
        #ver si fecha corte esta vacia y mandar diferente informacion
        if "fecha_corte" in data:
            newfecha=data['fecha_corte']
            fechaAct=conex.query_df("""select * from sc_alab.fecha_actual fa""")
            if newfecha.date() <= fechaAct.values[0][1].date():
                return {"success":True,
                        "Mensaje":"La fecha es igual o menor a la que tiene la DB",
                        "Consejo":"Asigne una fecha mayor"}, 201
            fechaAct=newfecha
            bandera=1

        if not "fecha_corte" in data:
            #llamado de Funcion de caducidad de boletas
            fechasFin=fechaCadBol(conex)
            
            #llamada de la funcion para comparar fechas
            fechaAct=compararFecha(conex, fechasFin)

        #Llamado de la FUncion para actualizar fecha de DB
        actFecha(conex,fechaAct,bandera)
    
        #Funcion de busqueda de boletas vencidas aun activas
        bolVen=bolCaducas(conex)
        
        #verifica si trajo datos, si viene vacia termina el proceso
        if bolVen.empty:
            return{"success":True, "Mensaje":"No hay boletas vencidas"}, 201
        
        #actualizar el status de boletas vencidas
        if not bolVen.empty:
           actStatBol(conex,bolVen) 
            
        return{"success":True, "Mensaje":"Status Cambiados"}, 201