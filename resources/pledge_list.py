from flask import request
from flask_restful import Resource
from db import DatabaseConnection


class PledgeList(Resource):
    def get(self):
        limit = request.args.get("limit")

        if limit == None:
            limit = 100

        df = DatabaseConnection().query_df("""
        select
            distinct on
            (b.id_boleta)
                concat_ws(' ', p.nom_1, p.nom_2, p.apellido_pat, p.apellido_mat) 
                as nombre_completo,
            e.dir_email,
            p.id_persona,
            t.no_tel,
            b.id_boleta,
            b.mont_prest_total,
            pe.tasa_interes,
            b.fecha_alta,
            b.fecha_fin,
            csb.id_cat_stats_bol,
            csb.nom_stats_bol,
            coalesce(pago_total, 0.0) as pago_total
        from
            sc_alab.personas p
        inner join sc_alab.boletas b 
                on
            p.id_persona = b.id_persona
        inner join sc_alab.telefonos t
                on
            t.id_persona = p.id_persona
        inner join sc_alab.email e
                on
            p.id_persona = e.id_persona
        inner join sc_alab.producto_emp pe
                on
            b.id_producto_emp = pe.id_producto_emp
        inner join sc_alab.cat_status_boleta csb 
                on
            csb.id_cat_stats_bol = b.id_cat_stats_bol
        left join (
            select
                rp.id_boleta ,
                SUM(rp.monto) as pago_total
            from
                sc_alab.registro_pago rp
            group by
                id_boleta 
                    ) pagos
                    on
            b.id_boleta = pagos.id_boleta
        order by
            b.id_boleta desc
        limit %s
        """, (limit,))

        df['fecha_alta'] = df['fecha_alta'].astype(str)
        df['fecha_fin'] = df['fecha_fin'].astype(str)

        return df.to_dict(orient='records')
