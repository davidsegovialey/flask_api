# Clientes (persona)

from flask import request, json
from flask_restful import Resource
from marshmallow import fields
from marshmallow.schema import Schema
from marshmallow.exceptions import ValidationError
from db import DatabaseConnection

class PaymentSchema (Schema):
    id_boleta = fields.Int(required=True)
    id_cat_stats_pago = fields.Int(requiered=True)
    fecha_pago = fields.DateTime(required=True)
    monto = fields.Float(requiered=True)


class Payment(Resource):
    def post(self):
        request_pay = request.get_json()
        try:
            pay = PaymentSchema().load(request_pay)
        except ValidationError as ERROR:
            error_messages=json.dumps(ERROR.messages)
            return {"success":False, "status_message":json.loads(error_messages)}, 400
        conex = DatabaseConnection()
        conex.open_connection()

        conex.cursor.execute("""INSERT INTO sc_alab.registro_pago
        (id_boleta, id_cat_stats_pago, fecha_pago, monto)
         VALUES(%s, %s,%s, %s) RETURNING id_res_pago;""" , (pay["id_boleta"], pay["id_cat_stats_pago"], pay["fecha_pago"], pay["monto"]) ) 

        id_res_pago = conex.cursor.fetchone()[0]
        conex.close_connection()
        return{"success":True,"id_res_pago":id_res_pago}, 201


    def get(self, id_boleta):
        df = DatabaseConnection().query_df(
            """
            SELECT rp.id_res_pago, rp.id_boleta, rp.id_cat_stats_pago, rp.fecha_pago,
            rp.monto, csp.nom_stats_pago 
            FROM sc_alab.registro_pago rp
            INNER JOIN sc_alab.cat_status_pago csp on csp.id_cat_stats_pago = rp.id_cat_stats_pago 
            WHERE rp.id_boleta = %s
            ORDER BY rp.fecha_pago desc
            """, (id_boleta,)
        )

        df['fecha_pago'] = df['fecha_pago'].astype(str)

        if df.empty:
            return {}, 404

        return df.to_dict(orient="records")

