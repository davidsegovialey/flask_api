# Este archivo maneja la conexion y desconexion a la base de datos

import sys
import pandas as pd
from pandas.core.frame import DataFrame
import psycopg2
import json

class DatabaseConnection():
    def __init__(self) -> None:
        try:
            with open("config.json") as f:
                data = json.load(f)
        except Exception as e:
            print(e)
                
        conf_db = data["db"]
        current_setting = conf_db["current_setting"]
        settings = conf_db["connections"][current_setting]
        self.user = settings["user"]
        self.password = settings["password"]
        self.host = settings["host"]
        self.dbname = settings["dbname"]
        self.is_connection_open = False
        

    # ejecutar una query sin resultado
    def query(self, query, params=None):
        try:
            self.open_connection()
            self.cursor.execute(query, params)
            self.close_connection()

        except Exception as err:
            self.print_psycopg2_exception(err)
            self.conn.rollback()
            self.close_connection()


    # ejecutar una query y obtener un dataframe con el resultado
    def query_df(self, query, params=None):
        try:
            self.open_connection()
            dt = pd.read_sql_query(query, self.conn, params=params)
            self.close_connection()
            return dt
        except Exception as err:
            print(err)
            self.conn.rollback()
            self.close_connection()
            return DataFrame()


    def open_connection(self):
        """abre la conexion y un cursor es creado, llamar a close_connection al finalizar"""
        if self.is_connection_open:
            self.close_connection()
        
        self.is_connection_open = True
        self.conn = psycopg2.connect(f"dbname={self.dbname} user={self.user} host={self.host} password={self.password}")
        self.cursor = self.conn.cursor()


    def close_connection(self):
        """cerrar la conexion y el cursor"""
        self.is_connection_open = False
        self.conn.commit()
        self.cursor.close()
        self.conn.close()


    # exceptions
    def print_psycopg2_exception(self, err):
        err_type, err_obj, traceback = sys.exc_info()
        line_num = traceback.tb_lineno
        print ("\npsycopg2 ERROR:", err, "on line number:", line_num)
        print ("psycopg2 traceback:", traceback, "-- type:", err_type)
        print ("pgerror:", err.pgerror)
        print ("pgcode:", err.pgcode, "\n")
        return {"error": err.diag, "detail": err.pgerror, "pgcode": err.pgcode}