# Main

from flask import Flask, app, jsonify, request
from flask_restful import Api
from flask_cors import CORS

from resources.auth import Auth
from resources.bactch2 import Batch2
from resources.cat_est_prenda import Cat_est_prenda
from resources.cat_ide import Cat_identificaciones
from resources.client import Client
from resources.client_list import ClientList
from resources.client_search import ClientSearch
from resources.cp import Cp
from resources.item_detail import ItemDetail
#from resources.detalle_prenda import Detalle_prenda
from resources.item_search import ItemSearch
from resources.pledge import Pledge
from resources.pledge_list import PledgeList
from resources.ticket import Ticket
from resources.ticket_search import TicketSearch
from resources.payment import Payment
from resources.batch import batch


def create_app():
    app = Flask(__name__)
    CORS(app)

    api = Api(app, catch_all_404s=True)
    app.url_map.strict_slashes = False

    # Routes
    api.add_resource(Auth, '/v1/auth')
    api.add_resource(Cat_identificaciones, '/v1/cat_ide')
    api.add_resource(Client, '/v1/client/', '/v1/client/<string:id>')
    api.add_resource(ClientSearch, '/v1/client_search')
    api.add_resource(ItemSearch, '/v1/item_search')
    api.add_resource(ItemDetail, '/v1/item_detail/<int:id>')
    api.add_resource(Cat_est_prenda, '/v1/cat_est_prenda')
    api.add_resource(TicketSearch, '/v1/ticket_search')
    api.add_resource(Payment, '/v1/payment', '/v1/payment/<int:id_boleta>')
    api.add_resource(batch, '/v1/batch')
    api.add_resource(Batch2, '/v1/batch2')
    api.add_resource(PledgeList, '/v1/pledge_list')
    api.add_resource(Cp, '/v1/cp')
    api.add_resource(Pledge, '/v1/pledge')
    api.add_resource(Ticket, '/v1/ticket/<int:id>')
    api.add_resource(ClientList, '/v1/client_list')

    #api.add_resource(Detalle_prenda, '/v1/detalle_prenda', '/v1/detalle_prenda/')

    return app


def register_error_handlers(app):
    @app.errorhandler(Exception)
    def handle_exception_error(e):
        return jsonify({'msg': 'Error 500: Internal server error'}), 500

    @app.errorhandler(405)
    def handle_405_error(e):
        return jsonify({'msg': 'Error 405: Method not allowed'}), 405

    @app.errorhandler(403)
    def handle_403_error(e):
        return jsonify({'msg': 'Error 403: Forbidden error'}), 403

    @app.errorhandler(404)
    def handle_404_error(e):
        return jsonify({'status_message': 'Recurso no encontrado.', 'success': 'false'}), 404


    # @app.after_request
    # def add_cors_headers(response):
    #     header = response.headers
    #     header['Access-Control-Allow-Origin'] = '*'
    #     return response


if __name__ == "__main__":
    app = create_app()
    app.run(threaded=True)
